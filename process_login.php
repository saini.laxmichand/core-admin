<?php
require("config/session.php");
require("config/database.php");
require("config/helper.php");

$role = validate_input(isset($_POST['role'])?$_POST['role']:'');
$email = validate_input(isset($_POST['email'])?$_POST['email']:'');
$phone = validate_input(isset($_POST['phone'])?$_POST['phone']:'');
$password = validate_input(isset($_POST['password'])?$_POST['password']:'');

if($_SERVER['REQUEST_METHOD']==='POST' && is_array($_POST) && !empty($phone) && !empty($password)){
	//get user id
	$email = '';
    $user_id = get_user_id_by_phone($phone);   
    //check user is valid or not
    $role_status = validate_user($email, $phone,$password,$role);
	if($role_status == 'User'){
		header("Location: user_dashboard.php");
		die;
	}else if($role_status == 'Vendor'){
		header("Location: vendor_dashboard.php");
		die;
	}else{       
		header( "Location: index.php?error=bG9naW4gZXJyb3I=" ); die;
	}
}else{
	header( "Location: index.php" ); die;
}
?>