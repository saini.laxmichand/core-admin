<?php
require("config/session.php");
require("config/database.php");
require("config/helper.php");


$userid = validate_input(isset($_POST['userid'])?$_POST['userid']:'');
$amount = validate_input(isset($_POST['amount'])?$_POST['amount']:'');

if($_SERVER['REQUEST_METHOD']==='POST' && is_array($_POST) && !empty($userid) && !empty($amount)){

	$user = get_userid($userid);
	//echo $amount;
	if($amount <= $user['wallet_limit']){
		
		$sql = "INSERT INTO wallet (user_id, wallet_balance) VALUES($userid,$amount)";
		$result = $GLOBALS['conn']->query($sql);
		header("Location: wallet.php?success=success");
		die;
	}else{
		header("Location: wallet.php?max_limit=max_limit");
		die;
	}		   
}else{
	header( "Location: wallet.php" ); die;
}
?>