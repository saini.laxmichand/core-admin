<?php
require("config/session.php");
require("config/database.php");
require("config/helper.php");

$name = validate_input(isset($_POST['name'])?$_POST['name']:'');
$role = validate_input(isset($_POST['role'])?$_POST['role']:'');
$phone = validate_input(isset($_POST['phone'])?$_POST['phone']:'');
$email = validate_input(isset($_POST['email'])?$_POST['email']:'');
$password = validate_input(isset($_POST['password'])?$_POST['password']:'');
$shop_name = validate_input(isset($_POST['shop_name'])?$_POST['shop_name']:'');
$shop_address = validate_input(isset($_POST['shop_address'])?$_POST['shop_address']:'');
$vendor_id = validate_input(isset($_POST['vendor_id'])?$_POST['vendor_id']:'');

if($_SERVER['REQUEST_METHOD']==='POST' && is_array($_POST) && !empty($email) && !empty($password)&& !empty($name) && !empty($phone)){
	
	//get user id
    $user_id = get_user_id($email);   
    //check user is valid or not
	if(!$user_id){
		$data = array(
			'role' => $role,
			'name' => $name,
			'phone' => $phone,
			'email' => $email,
			'password' => md5($password),
			'shop_name' => $shop_name,
			'shop_address' => $shop_address,
		);
		$lastuserId = insert('user', $data);
		if($vendor_id){
			$sql = "INSERT INTO vendor_user (vendor_id, user_id) VALUES($vendor_id,$lastuserId)";
			$result = $GLOBALS['conn']->query($sql);
		}
		
		header("Location: user.php?success=success");
		die;	
	}else{
		header("Location: user.php?error=duplicate");
		die;	
	}
}else{
	header( "Location: user.php" ); die;
}
?>