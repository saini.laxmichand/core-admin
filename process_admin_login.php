<?php
require("config/session.php");
require("config/database.php");
require("config/helper.php");

$role = validate_input(isset($_POST['role'])?$_POST['role']:'');
$email = validate_input(isset($_POST['email'])?$_POST['email']:'');
$password = validate_input(isset($_POST['password'])?$_POST['password']:'');

if($_SERVER['REQUEST_METHOD']==='POST' && is_array($_POST) && !empty($email) && !empty($password)){

	$phone = '';
	//get user id
    $user_id = get_user_id($email);   
    //check user is valid or not
    $role_status = validate_user($email,$password, $phone, $role);
	if($role_status == 'Admin'){
		header( "Location: admin_dashboard.php" ); die;		
	}else{       
		header("Location: admin_login.php?error=bG9naW4gZXJyb3I=" ); die;
	}
}else{
	header( "Location: index.php" ); die;
}
?>