<?php
$error_code = @$_GET['error'];
if ($error_code == ERROR_USER_DUPLICATE) {
    display_error('alert-danger', ERROR_MSG_USER_DUPLICATE);
}

$error_code = @$_GET['success'];
if ($error_code == SUCCESS_USER_SAVE) {
    display_error('alert-success', SUCCESS_USER_SAVE_MSG);
}

$sql = "SELECT * FROM user where role in('Vendor')";
$data = fetch_custom($sql);
?>
<a href="user.php?action=user"><i class="fa fa-arrow-left"></i> Back</a>
<h2 class="mt-5">User</h2>
<form action="user_create.php" method="POST">
    <div class="form-group">
        <label for="name">Role:</label>
        <select class="form-control" id="role" name="role" onchange="showDiv(this)" required>
            <option value="">Select</option>
            <option value="User"> User </option>
            <option value="Vendor"> Vendor </option>
        </select>
    </div>

    <div class="form-group" id="vendordiv" style="display: none;">
        <label for="name">Vendor:</label>
        <select class="form-control" id="vendor_id" name="vendor_id">
            <option value="">Select</option>
            <?php foreach ($data as $user) { ?>
                <option value="<?php echo isset($user['id']) ? $user['id'] : ''; ?>"> <?php echo isset($user['name']) ? $user['name'] : ''; ?> </option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" id="name" name="name" required>
    </div>

    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" id="email" name="email" required>
    </div>

    <div class="form-group">
        <label for="phone">Phone:</label>
        <input type="tel" id="phone" name="phone" class="form-control" placeholder="Phone(987654321)" pattern="[0-9]{10}" required>
    </div>

    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password" name="password" required>
    </div>

    <div class="form-group showShop" style="display: none;">
        <label for="shop_name">Shop Name:</label>
        <input type="text" class="form-control" id="shop_name" name="shop_name">
    </div>

    <div class="form-group showShop" style="display: none;">
        <label for="shop_address">Shop Address:</label>
        <input type="text" class="form-control" id="shop_address" name="shop_address">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
<script>
    function showDiv(select) {
        if (select.value == 'Vendor') {
            $('.showShop').show();
        } else {
            $('.showShop').hide();
        }

        if (select.value == 'User') {
            $('#vendordiv').show();
        } else {
            $('#vendordiv').hide();
        }
    }
</script>