<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <?php

      $sql = "SELECT * FROM user";
      $data =  $GLOBALS['conn']->query($sql);
      
      
      ?>
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo ($data->num_rows); ?></h3>

          <p>User/Vendor Registrations</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="user.php?action=user" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->