<?php

$sql = "SELECT * FROM user where role in('User','Vendor')";
$data = fetch_custom($sql);
?>
<?php $error_code = @$_GET['max_limit'];
if ($error_code == MAX_LIMIT) {
    display_error('alert-danger', MAX_LIMIT_MSG);
} ?>
<h2 class="mt-5">User List <span style="float: right;font-size:16px;"><a href="user.php?action=userCreate">User Create <i class="fa fa-plus-circle" aria-hidden="true"></i></a> </span> </h2>
<table class="table table-striped" id="userlist">
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Role</th>
            <th>Wallet</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($data as $user) {
        ?>
            <tr>
                <td><?= $i; ?></td>
                <td><?= $user['name']; ?></td>
                <td><?= $user['email']; ?></td>
                <td><?= $user['phone']; ?></td>
                <td><?= $user['role']; ?></td>
                <td>
                    <?php
                    
                        echo '<a href="wallet.php?userid=' . $user['id'] . '"> check Wallet</a>';
                    
                    ?>
                </td>
            </tr>
        <?php $i++;
        } ?>
        <!-- Additional rows for more users can be added here -->
    </tbody>
</table>
<script src="https://code.jquery.com/jquery-3.7.1.js"></script>
<script>
    $(document).ready(function() {
        new DataTable('#userlist');
    });
</script>