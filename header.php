<header class="main-header">
    <!-- Logo -->
    <a href="admin_dashboard.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">User</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Welcome</b> </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $_SESSION['NAME'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                            <p>
                                <?php echo $_SESSION['NAME'] ?>

                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li class="active treeview">
                <a href="<?php
                            if (isset($_SESSION) && $_SESSION['ROLE'] == 'Admin') {
                                echo 'admin_dashboard.php';
                            } else if (isset($_SESSION) && $_SESSION['ROLE'] == 'User') {
                                echo 'user_dashboard.php';
                            } else if (isset($_SESSION) && $_SESSION['ROLE'] == 'Vendor') {
                                echo 'vendor_dashboard.php';
                            }
                            ?>?action=dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php if (isset($_SESSION) && $_SESSION['ROLE'] == 'Admin') { ?>
                <li class="active treeview">
                    <a href="user.php?action=user">
                        <i class="fa fa-user"></i> <span>User</span>
                    </a>
                </li>
            <?php } ?>

            <?php if (isset($_SESSION) && $_SESSION['ROLE'] !== 'Admin') { ?>
                <li class="active treeview">
                    <a href="wallet.php">
                        <i class="fa fa-money"></i> <span>Wallet</span>
                    </a>
                </li>
            <?php } ?>
            <li class="treeview">
                <a href="logout.php">Sign out</a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>