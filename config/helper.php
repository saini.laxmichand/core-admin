<?php
/*
 * |-------------------------------------------------------
 * | Validate Input
 * |-------------------------------------------------------
 */
function validate_input($data) {
  	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}

/*
 * |-------------------------------------------------------
 * | Display error message
 * |-------------------------------------------------------
 */
function display_error($class_name,$message) {
  	echo "<div class='alert $class_name'>$message</div>";
}

/*
 * |-------------------------------------------------------
 * | Fetch single data
 * |-------------------------------------------------------
 */
function fetch_single($table,$field,$key,$value) {
  	$sql = "SELECT $field FROM $table WHERE $key = '$value' LIMIT 1";
	$result = $GLOBALS['conn']->query($sql);
	if ($result->num_rows > 0) {
    	$data = $result->fetch_assoc();
    	return $data;
	}else{
		return FALSE;
	}	
}

/*
 * |-------------------------------------------------------
 * | Fetch multiple data
 * |-------------------------------------------------------
 */
function fetch_multiple($table,$field,$key,$value) {
  	$sql = "SELECT $field FROM $table WHERE $key = '$value'";
	$result = $GLOBALS['conn']->query($sql);
	if ($result->num_rows > 0) {
    	$data = mysqli_fetch_all($result,MYSQLI_ASSOC);
    	return $data;
	}else{
		return FALSE;
	}	
}

/*
 * |-------------------------------------------------------
 * | Fetch data with custom query
 * |-------------------------------------------------------
 */
function fetch_custom($sql) {
	$result = $GLOBALS['conn']->query($sql);
	if ($result->num_rows > 0) {
    	$data = mysqli_fetch_all($result,MYSQLI_ASSOC);
    	return $data;
	}else{
		return FALSE;
	}	
}

/*
 * |-------------------------------------------------------
 * | Insert array of data
 * |-------------------------------------------------------
 */
function insert($table,$data) {
	// retrieve the keys of the array (column titles)
    $fields = array_keys($data);
    // build the query
    $sql = "INSERT INTO ".$table." (`".implode('`,`', $fields)."`) VALUES('".implode("','", $data)."')";
	if ($GLOBALS['conn']->query($sql) === TRUE) {
		$last_inserted_id = $GLOBALS['conn']->insert_id;
		return $last_inserted_id;
		//return true;
	} else {
		return false;
	}
}

/*
 * |-------------------------------------------------------
 * | Get user id by using email id
 * |-------------------------------------------------------
 */
function get_user_id($email){
	$data = fetch_single('user','id','email',$email);
	if($data){
		return $data;
	}else{
		return FALSE;
	}
}

function get_userid($id)
{
	$data = fetch_single('user', '*', 'id', $id);
	if ($data) {
		return $data;
	} else {
		return FALSE;
	}
}

function get_user_id_by_phone($phone)
{
	$data = fetch_single('user', 'id', 'phone', $phone);
	if ($data) {
		return $data;
	} else {
		return FALSE;
	}
}


/*
 * |-------------------------------------------------------
 * | Validate user login
 * |-------------------------------------------------------
 */
function validate_user($email, $phone, $password, $role){
	//encript password to md5
	$password = md5($password);
	//echo $password;die;
	if($role == "Admin"){
		$table = 'admin';
		echo $query = "SELECT * FROM $table WHERE email='" . $email . "' AND password='" . $password . "' LIMIT 1";
	}else{
		$table = 'user';
		$query = "SELECT * FROM $table WHERE phone='" . $phone . "' AND password='" . $password . "' AND role='" . $role . "' LIMIT 1";
	}
	$sql = $query;
	$data = fetch_custom($sql);
	
	if($data){
		//fill the result to session variable
		$_SESSION['MEMBER_ID'] = $data[0]['id'];
		$_SESSION['NAME'] = $data[0]['name'];
		$_SESSION['ROLE'] = $data[0]['role'];
		return $data[0]['role'];
	}else{
		return FALSE;
	}
}

/*
 * |-------------------------------------------------------
 * | User logout
 * |-------------------------------------------------------
 */
function logout_user(){
	unset($_SESSION['MEMBER_ID']);
	unset($_SESSION['NAME']);
	unset($_SESSION['ROLE']);
}

?>