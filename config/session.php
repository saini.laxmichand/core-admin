<?php
session_start();

//function to check if the session variable member_id is on set
function logged_in() {
	return isset($_SESSION['MEMBER_ID']);
}

//Check user already logged in or not
function confirm_logged_in() {
	if (!logged_in()) {
		header( "Location: index.php" ); die;
	}
}

function check_role()
{
	if ($_SESSION["ROLE"] !== "Admin") {
		if ($_SESSION["ROLE"] === "User") {
			header("Location: user_dashboard.php"); // Redirect to user dashboard or other page
			exit();
		} elseif ($_SESSION["ROLE"] === "Vendor") {
			header("Location: vendor_dashboard.php"); // Redirect to user dashboard or other page
			exit();
		}
	}
}
function check_admin()
{
	if ($_SESSION["ROLE"] !== "Admin") {
		header("Location: index.php");
		return;
	}
}
?>