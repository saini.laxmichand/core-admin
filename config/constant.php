<?php
define ( 'PROJECT_MODULE', 'Admin' );
define ( 'PROJECT_TITLE', 'Hash Coder' );
define ( 'PROJECT_SHORT_TITLE', 'HashCoder' );
define ( 'PROJECT_PREFIX', 'HC' );

define ( 'ERROR_CODE_LOGIN', 'bG9naW4gZXJyb3I=' );
define ( 'ERROR_MSG_LOGIN', 'The email address or password you entered is not valid' );
define ( 'ERROR_CODE_BLOCKED', 'bG9naW4gYmxvY2tlZA==' );
define ( 'ERROR_MSG_BLOCKED', 'Sorry! your account is temporary blocked. Please try after 10 minutes.' );

define('ERROR_USER_DUPLICATE', 'duplicate');
define('ERROR_MSG_USER_DUPLICATE', 'Already exist.');

define('SUCCESS_USER_SAVE', 'success');
define('SUCCESS_USER_SAVE_MSG', 'Success.');

define('MAX_LIMIT', 'max_limit');
define('MAX_LIMIT_MSG', 'Can not add more then 40000.');
?>